import torch
from torch.utils.data import DataLoader
from depth_dataset import DepthDataset
from models import get_model
from torch import optim
from torch import nn
from torch.utils.tensorboard import SummaryWriter
from torchvision.utils import make_grid
from time import time


def get_device():
    if torch.cuda.is_available():
        dev = "cuda"
        print("Using cuda")
    else:
        dev = "cpu"
        print("Using cpu")
    return torch.device(dev)


def train(model, data_loader, optimizer, loss_func, device):
    train_loss = 0
    batch_number = 0

    for xb, yb in data_loader:
        xb = xb.to(device)
        yb = yb.to(device)

        out = model(xb)
        loss = loss_func(out, yb)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        train_loss += loss.item()

        batch_number += 1
        print(
            f"training... {batch_number}/{len(data_loader)}, loss: {train_loss/batch_number}",
            end="\r",
        )

    return train_loss / len(data_loader)


def validate(model, data_loader, loss_func, device):
    test_loss = 0
    batch_number = 0

    with torch.no_grad():
        for xb, yb in data_loader:
            xb = xb.to(device)
            yb = yb.to(device)

            out = model(xb)
            loss = loss_func(out, yb)
            test_loss += loss.item()

            batch_number += 1
            print(
                f"validating... {batch_number}/{len(data_loader)}, loss: {test_loss/batch_number}",
                end="\r",
            )

    return test_loss / len(data_loader)


def fit(epochs, model, loss_func, optimizer, train_dl, valid_dl, device):
    elapsed_time = 0
    #
    # TENSORBOARD INIT
    #
    tensorboard_writer = SummaryWriter(comment="NO_COMMENT")
    # tensorboard_image = iter(valid_dl).next()[0][0]
    # tensorboard_expected_image = 1.0 - tensorboard_image

    for epoch in range(epochs):
        start_time = time()
        train_loss = train(model, train_dl, optimizer, loss_func, device)
        train_time = time() - start_time

        start_time = time()
        valid_loss = validate(model, valid_dl, loss_func, device)
        validation_time = time() - start_time

        #
        # TENSORBOARD
        #
        tensorboard_writer.add_scalar("train_loss", train_loss, epoch)
        tensorboard_writer.add_scalar("valid_loss", valid_loss, epoch)
        # grid = make_grid(torch.tensor([tensorboard_image.numpy(), tensorboard_expected_image.numpy()]))
        # tensorboard_writer.add_image('expected_image', grid, epoch)

        # TODO: Better way to checkpoint models
        torch.save(model, f"./m{epoch}")

        print(
            f"epoch: {epoch} - train loss: {train_loss} - test loss: {valid_loss} - train time: {round(train_time, 2)} - test time: {round(validation_time, 2)}"
        )
        elapsed_time += round(train_time, 2) + round(validation_time, 2)
        print(
            "ELAPSED TIME (minutes):",
            round(elapsed_time / 60, 2),
            "REMAINING TIME (minutes):",
            round((epochs - (epoch + 1)) * (train_time + validation_time) / 60, 2),
        )


def get_data(
    train_directories,
    test_directories,
    batch_size,
    test_batch_size=None,
    num_workers=14,
):
    test_batch_size = test_batch_size if test_batch_size is not None else batch_size

    train_dataset = DepthDataset(train_directories)
    test_dataset = DepthDataset(test_directories)

    train_dataloader = DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers
    )
    test_dataloader = DataLoader(
        test_dataset, batch_size=test_batch_size, num_workers=num_workers
    )

    return train_dataloader, test_dataloader


def run():
    EPOCHS = 7
    BATCH_SIZE = 28
    MODEL_LOAD_DIRECTORY = None
    training_directory = ["/media/juanpablo/DATASETS/dataset/360_noshake_1/"]
    validation_directory = ["/media/juanpablo/DATASETS/dataset/valid/"]

    device = get_device()

    if MODEL_LOAD_DIRECTORY is None:
        model = get_model().to(device)
    else:
        model = torch.load(MODEL_LOAD_DIRECTORY).to(device)

    train_dataloader, test_dataloader = get_data(
        training_directory, validation_directory, BATCH_SIZE
    )

    loss_func = nn.MSELoss()
    optimizer = optim.Adam(model.parameters())

    fit(EPOCHS, model, loss_func, optimizer, train_dataloader, test_dataloader, device)


if __name__ == "__main__":
    run()
