import os
import torch
from PIL import Image
import augmentator
from torchvision import transforms
from torch.utils.data import Dataset


class DepthDataset(Dataset):
    def __init__(
        self,
        directories,
        transform=transforms.ToTensor(),
        crop=0,
        rotate=0,
        translate=0,
        shear=0,
        h_flip=0.5,
        imagesical=0,
    ):
        self.directories = directories
        self.game_paths = []
        self.depth_paths = []
        self.transform = transform

        self.crop = crop
        self.rotate = rotate
        self.translate = translate
        self.shear = shear
        self.h_flip = h_flip

        for directory in directories:
            game_directory = os.path.join(directory, "game")
            depth_directory = os.path.join(directory, "depth")

            for subdirectory in os.listdir(game_directory):
                game_subdirectory_full_path = os.path.join(game_directory, subdirectory)
                depth_subdirectory_full_path = os.path.join(
                    depth_directory, subdirectory
                )
                for image in os.listdir(depth_subdirectory_full_path):
                    full_game_image_path = os.path.join(
                        game_subdirectory_full_path, image.replace("_d", "_g")
                    )
                    full_depth_image_path = os.path.join(
                        depth_subdirectory_full_path, image
                    )
                    self.game_paths.append(full_game_image_path)
                    self.depth_paths.append(full_depth_image_path)

    def __getitem__(self, n):
        game_image = Image.open(self.game_paths[n])
        depth_image = Image.open(self.depth_paths[n])

        game_image, depth_image = augmentator.augmentation(
            [game_image, depth_image],
            crop=self.crop,
            rotate=self.rotate,
            horizontal=self.h_flip,
        )

        return self.transform(game_image), self.transform(depth_image.convert("L"))

    def __len__(self):
        return len(self.game_paths)
