from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from delete_confirmation_popup import DeleteConfirmationPopup


class ClearPanelButtonBar(BoxLayout):
    """ Boxlayout having clearing button"""

    def __init__(self, image_panel):
        super().__init__()
        self.orientation = "horizontal"
        self.size_hint = 1, 0.05
        self.add_widget(Label())
        self.add_widget(ClearPanelButton(image_panel))
        self.add_widget(Label())


class ClearPanelButton(Button):
    """ Button clearing whole panel"""

    def __init__(self, image_panel):
        super().__init__()
        self.image_panel = image_panel
        self.size_hint = 0.4, 1
        self.text = "Clear"
        self.bind(on_press=self.show_popup)

    def show_popup(self, _):
        DeleteConfirmationPopup(image_panel=self.image_panel, clear_all=True)
