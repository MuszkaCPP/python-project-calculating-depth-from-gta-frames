from kivy.uix.button import Button
from PIL import Image
from kivy.uix.image import Image as kivy_img
from torchvision import transforms
import torch
from kivy.graphics.texture import Texture
import numpy


class PredictButton(Button):
    """PREDICTION"""

    def __init__(self, text_panel, image_panel):
        super().__init__()
        self.text = "Predict"
        self.text_panel = text_panel
        self.image_panel = image_panel
        self.model_name = None
        self.model = None
        self.pred_images_paths = []
        self.device = self.get_device()

    def set_pred_image_path(self, paths):
        for path in paths:
            self.pred_images_paths.append(path)

    def on_release(self):
        if self.model is None:
            self.text_panel.text += "No model chosen." + "\n"
        else:
            self.text_panel.text += "Prediction on model: " + self.model_name + "\n"
            self.make_prediction()

    def make_prediction(self):
        if len(self.pred_images_paths) > 0:
            post_prediction = []
            #
            # Adding Image predictions to
            # post_prediction list
            #
            for pred_image_path in self.pred_images_paths:
                pil_image = Image.open(pred_image_path).resize((224,224)).convert('RGB')
                transformations = transforms.Compose([transforms.ToTensor()])
                image = transformations(pil_image).to(self.device)
                with torch.no_grad():
                    prediction = self.model(image.view([1, 3, 224, 224]))
                arr = prediction[0][0].detach().cpu().numpy()
                pred_img = Texture.create(size=(224, 224))
                pred_img.blit_buffer(
                    numpy.flipud(arr).flatten(), bufferfmt="float", colorfmt="luminance"
                )

                post_prediction += [kivy_img(texture=pred_img)]
            #
            # Putting prediciton image_panel
            #
            self.image_panel.refresh_pred(post_prediction)
        else:
            self.text_panel.text += "No image choosen for prediction\n"

    def change_model(self, model, model_name):
        self.model = model.to(self.device)
        self.model_name = model_name

    def get_device(self):
        if torch.cuda.is_available():
            dev = "cuda"
        else:
            dev = "cpu"
        return torch.device(dev)
