from kivy.uix.button import Button
from save_popup import SavePopup


class SavePredBtn(Button):
    """SAVE PREDICTIONS"""

    def __init__(self, text_panel, image_panel):
        super().__init__()
        self.text = "Save predictions"
        self.text_panel = text_panel
        self.image_panel = image_panel

    def on_release(self):
        SavePopup(self.text_panel, self.image_panel)
