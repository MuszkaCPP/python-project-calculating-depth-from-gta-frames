from PIL import Image
from PIL import ImageGrab
import win32gui
import time
import numpy as np
import os
from pathlib import Path
import argparse
import torch
from torchvision import transforms


WINDOW_NAME = "Grand Theft Auto V"
TARGET_SIZE = 224


def capture_game_images():
    hwnd = None

    while not hwnd:
        hwnd = win32gui.FindWindow(None, WINDOW_NAME)

    try:
        # win32gui.SetForegroundWindow(hwnd)
        dimensions = win32gui.GetWindowRect(hwnd)

        image = ImageGrab.grab(dimensions)

        width, height = image.width, image.height

        game_image0 = image.crop((0, 0, width // 4, height))
        game_image0 = game_image0.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
        game_image0 = transforms.ToTensor()(game_image0)

        game_image1 = image.crop((width // 4 + 1, 0, 2 * width // 4, height))
        game_image1 = game_image1.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
        game_image1 = transforms.ToTensor()(game_image1)

        game_image2 = image.crop((2 * width // 4 + 1, 0, 3 * width // 4, height))
        game_image2 = game_image2.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
        game_image2 = transforms.ToTensor()(game_image2)

        game_image3 = image.crop((3 * width // 4 + 1, 0, width, height))
        game_image3 = game_image3.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
        game_image3 = transforms.ToTensor()(game_image3)

        return [game_image0, game_image1, game_image2, game_image3]
    except:
        print("Cannot capture window!")


def capture_game_image():
    hwnd = None

    while not hwnd:
        hwnd = win32gui.FindWindow(None, WINDOW_NAME)

    try:
        # win32gui.SetForegroundWindow(hwnd)
        dimensions = win32gui.GetWindowRect(hwnd)

        image = ImageGrab.grab(dimensions)

        width, height = image.width, image.height

        game_image0 = image.crop((0, 0, width, height))
        game_image0 = game_image0.resize((TARGET_SIZE, TARGET_SIZE), Image.BICUBIC)
        game_image0 = transforms.ToTensor()(game_image0)

        return [game_image0]
    except:
        print("Cannot capture window!")
