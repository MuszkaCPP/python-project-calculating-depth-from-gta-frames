import torch
from torchvision import transforms
import depth_dataset as dd
import models
import numpy as np
import cv2
from models import UnetModel

DD = dd.DepthDataset(["./"])
model = UnetModel()  # są parametry domyślne, w razie potrzeby dodam ich więcej
img, img2 = DD.__getitem__(0)
img = torch.from_numpy(img)
img = img.view(1, 3, 224, 224)
pred = model(img.float())
print(pred)
