import torch
import time

from torch import nn
from torch import Tensor
from torchvision import datasets, transforms
from models import MnistModel
from torch import optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchvision.utils import make_grid


def get_device():
    if torch.cuda.is_available():
        dev = "cuda:0"
        print("You have cuda device available")
    else:
        dev = "cpu"
        print("No cuda device detected, using cpu")
    return torch.device(dev)


def get_generators(batch_size) -> (DataLoader, DataLoader):

    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))]
    )

    train_set = datasets.MNIST("", download=True, train=True, transform=transform)
    test_set = datasets.MNIST("", download=True, train=False, transform=transform)

    train_loader = torch.utils.data.DataLoader(
        train_set, batch_size=batch_size, shuffle=True
    )
    test_loader = torch.utils.data.DataLoader(
        test_set, batch_size=batch_size, shuffle=True
    )

    return train_loader, test_loader


def fit(model, generator, optimizer, loss_fn, device):
    train_loss = 0

    for images, labels in generator:
        images = images.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()

        output = model(images)
        loss = loss_fn(output, labels)
        loss.backward()
        optimizer.step()
        train_loss += loss.item()

    return train_loss / len(generator)


def validate(model, generator, loss_fn, device):
    test_loss = 0

    with torch.no_grad():
        for images, labels in generator:
            images = images.to(device)
            labels = labels.to(device)

            output = model(images)
            loss = loss_fn(output, labels)
            test_loss += loss.item()

    return test_loss / len(generator)


def main():
    EPOCHS = 7
    BATCH_SIZE = 64
    TB_COMMENT = "NO_COMMENT"

    device = get_device()
    model = MnistModel().to(device)
    loss_fn = nn.NLLLoss()
    optimizer = optim.Adam(model.parameters())
    train_gen, test_gen = get_generators(BATCH_SIZE)
    tensorboard_writer = SummaryWriter(comment=TB_COMMENT)
    tensorboard_image = iter(test_gen).next()[0][0]
    tensorboard_expected_image = 1.0 - tensorboard_image

    for epoch in range(EPOCHS):

        start_time = time.time()
        train_loss = fit(model, train_gen, optimizer, loss_fn, device)
        train_time = time.time() - start_time

        start_time = time.time()
        test_loss = validate(model, test_gen, loss_fn, device)
        test_time = time.time() - start_time

        tensorboard_writer.add_scalar("train_loss", train_loss, epoch)
        tensorboard_writer.add_scalar("test_loss", test_loss, epoch)
        grid = make_grid(
            torch.tensor(
                [tensorboard_image.numpy(), tensorboard_expected_image.numpy()]
            )
        )
        tensorboard_writer.add_image("expected_image", grid, epoch)
        # tensorboard_writer.add_image('expected_image', 1.0 - tensorboard_image, epoch)
        print(
            f"epoch: {epoch} - train loss: {train_loss} - test loss: {test_loss} - train time: {round(train_time, 2)} - test time: {round(test_time, 2)}"
        )


if __name__ == "__main__":
    main()
